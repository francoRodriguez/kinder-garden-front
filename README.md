<h1 id="argon-design-system">Kinder Garden System</h1>

<p>This proyect are created with Argon Design System for Bootstrap 4 of VUE</p>

<h4 id="example-pages">Example Pages of Argon Design</h4>

<ul>
  <li><a href="#demo">Demo</a></li>
  <li><a href="#quick-start">Quick Start</a></li>
  <li><a href="#documentation">Documentation</a></li>
  <li><a href="#file-structure">File Structure</a></li>
  <li><a href="#browser-support">Browser Support</a></li>
  <li><a href="#resources">Resources</a></li>
  <li><a href="#reporting-issues">Reporting Issues</a></li>
  <li><a href="#technical-support-or-questions">Technical Support or Questions</a></li>
  <li><a href="#licensing">Licensing</a></li>
  <li><a href="#useful-links">Useful Links</a></li>
</ul>

<h2 id="demo">Demo</h2>

<ul>
  <li><a href="https://demos.creative-tim.com/vue-argon-design-system">Index Page</a></li>
  <li><a href="https://demos.creative-tim.com/vue-argon-design-system/#/landing">Landing page</a></li>
  <li><a href="https://demos.creative-tim.com/vue-argon-design-system/#/profile">Profile Page</a></li>
  <li><a href="https://demos.creative-tim.com/vue-argon-design-system/#/login">Login Page</a></li>
  <li><a href="https://demos.creative-tim.com/vue-argon-design-system/#/register">Register Page</a></li>
</ul>

<p><a href="https://demos.creative-tim.com/argon-design-system">View More</a></p>

<h2 id="quick-start">Quick start</h2>

<ul>
  <li><a href="https://github.com/creativetimofficial/vue-argon-design-system/archive/master.zip">Download from Github</a>.</li>
  <li><a href="https://www.creative-tim.com/product/vue-argon-design-system">Download from Creative Tim</a>.</li>
  <li>Clone the repo: <code class="highlighter-rouge">git clone https://github.com/creativetimofficial/vue-argon-design-system.git</code>.</li>
</ul>

<h2 id="documentation">Documentation</h2>

<p>The documentation for the Vue Argon Design System is hosted at our <a href="https://demos.creative-tim.com/vue-argon-design-system">website</a>.</p>

<h2 id="file-structure">File Structure</h2>

<p>Within the download you’ll find the following directories and files:</p>

<div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>argon/
|-- vue-argon-design-system
    |-- App.vue
    |-- main.js
    |-- router.js
    |-- assets
    |   |-- scss
    |   |   |-- argon.scss
    |   |   |-- bootstrap
    |   |   |-- custom
    |   |-- vendor
    |       |-- font-awesome
    |       |   |-- css
    |       |   |   |-- font-awesome.css
    |       |   |   |-- font-awesome.min.css
    |       |   |-- fonts
    |       |       |-- FontAwesome.otf
    |       |       |-- fontawesome-webfont.eot
    |       |       |-- fontawesome-webfont.svg
    |       |       |-- fontawesome-webfont.ttf
    |       |       |-- fontawesome-webfont.woff
    |       |       |-- fontawesome-webfont.woff2
    |       |-- nucleo
    |           |-- css
    |           |   |-- nucleo-svg.css
    |           |   |-- nucleo.css
    |           |-- fonts
    |               |-- nucleo-icons.eot
    |               |-- nucleo-icons.svg
    |               |-- nucleo-icons.ttf
    |               |-- nucleo-icons.woff
    |               |-- nucleo-icons.woff2
    |-- components
    |   |-- Badge.vue
    |   |-- BaseButton.vue
    |   |-- BaseCheckbox.vue
    |   |-- BaseInput.vue
    |   |-- BaseNav.vue
    |   |-- BaseRadio.vue
    |   |-- BaseSlider.vue
    |   |-- BaseSwitch.vue
    |   |-- Card.vue
    |   |-- CloseButton.vue
    |   |-- Icon.vue
    |   |-- NavbarToggleButton.vue
    |-- layout
    |   |-- AppFooter.vue
    |   |-- AppHeader.vue
    |-- plugins
    |   |-- argon-kit.js
    |   |-- globalComponents.js
    |   |-- globalDirectives.js
    |-- views
        |-- Components.vue
        |-- Landing.vue
        |-- Login.vue
        |-- Profile.vue
        |-- Register.vue
        |-- components
            |-- BasicElements.vue
            |-- Carousel.vue
            |-- CustomControls.vue
            |-- DownloadSection.vue
            |-- Examples.vue
            |-- Hero.vue
            |-- Icons.vue
            |-- Inputs.vue
            |-- JavascriptComponents.vue
            |-- Navigation.vue

</code></pre></div></div>

<h2 id="browser-support">Browser Support</h2>

<p>At present, we officially aim to support the last two versions of the following browsers:</p>

<p><img src="https://s3.amazonaws.com/creativetim_bucket/github/browser/chrome.png" width="64" height="64" />
<img src="https://s3.amazonaws.com/creativetim_bucket/github/browser/firefox.png" width="64" height="64" />
<img src="https://s3.amazonaws.com/creativetim_bucket/github/browser/edge.png" width="64" height="64" />
<img src="https://s3.amazonaws.com/creativetim_bucket/github/browser/safari.png" width="64" height="64" />
<img src="https://s3.amazonaws.com/creativetim_bucket/github/browser/opera.png" width="64" height="64" /></p>

<h2 id="resources">How to use</h2>

<p>In proyect folder run npm run serve and go to link http://localhost:8080/#/</p>
